from testflows.core import *


@TestScenario
def scenario(self):
    """This is my scenario description"""
    pass


@TestStep(Given)
def ldap_user(self, name):
    """Create user inside a database and then delete it"""
    try:
        with Given(f"I create user {name}"):
            pass
        yield
    finally:
        with Finally(f"I delete user {name}"):
            pass


with Module("Test", description="This is module description"):
    with Given("I have a user"):
        ldap_user(name="my_user")
