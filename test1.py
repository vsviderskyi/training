import random

from testflows.asserts import values, error
from testflows.core import *


@TestModule
def regression(self):
    with Scenario("Test 1"):
        with Step("Step 1.1"):
            pass

    with Scenario("Test 2"):
        with Step("Step 2.1"):
            pass

    with Scenario("Test 3"):
        with Step("Step 3.1"):
            pass

#  python3 test.py -- help
#  python3 test1.py --start "/regression/test 2/*"
#  python3 test1.py --only "/regression/:/step 2:"  : match anything except slash
#  python3 test1.py --pause-before "/regression/test 2" pause and then press enter after viewing
#  python3 test1.py --repeat "/regression/test 2" ,2  if test is not repeatable it is inline
#  python3 test1.py

# ok() - OK, fail() -> Fail, err() - Error, null() - Null, xok(), xfail, xnull, note, reason(only for x... tests)
#


@TestModule
def regression1(self):
    with Given("I create database"):
        pass
    with Scenario("test 1", flags=TE):  # MANDATORY
        with When("step 1.2"):
            pass
        with When("step 1.2"):
            pass
        with When("step 1.2.2"):
            pass
        with Finally("step 1.3"):
            pass


@TestScenario
def test_2(self):
    with Step("step 2.1"):
        if random.random() > 0.5:
            fail("failed")


@TestScenario
def test_3(self):
    with Step("Step 3.1", flags=TE):
        expected = 2
        assert 3 == expected, "not matched"

    with Step("Step 3.2"):
        expected = 2
        l = []
        with values() as that:
            assert that(l.append(1)) == expected, error()


def test_4(self):
    with Step("Step 4.1"):
        fail("failed")


xfails = {  # Specifies which tests are intended to fail
    "test_4": [(Fail, "some reason")]
}


@TestModule
def regression2(self):
    with Scenario("test 1", flags=TE):
        with Step("Step 1.1"):
            fail("failed")

    Scenario(run=test_4, flags=TE)

    with Scenario("test 3", flags=TE):
        with Step("Step 3.1"):
            pass
# Instead of pass possible options:
# fail("failed")
# err("error")


if main():
    # regression()
    # regression1()
    # regression2()
    Module(test=regression2, xfails=xfails)()
